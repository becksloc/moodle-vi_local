<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://elearning.humg.edu.vn
 *
 * @package    mod
 * @subpackage quiz
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['attemptfirst'] = 'Lần làm bài đầu tiên';
$string['attemptlast'] = 'Lần làm bài cuối cùng';
$string['attemptsnum'] = 'Bài làm: {$a}';
$string['attemptstate'] = 'Trạng thái';
$string['backtocourse'] = 'Quay lại khóa học';
$string['endtest'] = 'Kết thúc làm bài ...';
$string['everynquestions'] = '{$a} câu hỏi trên một trang';
$string['everyquestion'] = 'Mỗi câu hỏi trên một trang';
$string['finishattemptdots'] = 'Kết thúc làm bài ...';
$string['gradingmethod'] = 'Cách tính điểm: {$a}';
$string['inprogress'] = 'Đang tiến hành';
$string['layout'] = 'Bố cục trang thi';
$string['mustbesubmittedby'] = 'Bài làm này phải được nộp trước {$a}.';
$string['navigatenext'] = 'Trang sau';
$string['navigateprevious'] = 'Trang trước';
$string['navmethod_free'] = 'Tự do';
$string['navmethod_seq'] = 'Tuần tự';
$string['neverallononepage'] = 'Tất cả câu hỏi trên một trang';
$string['newpage'] = 'Trang mới';
$string['noattemptsfound'] = 'Không tìm thấy bài nộp';
$string['overduehandling'] = 'Khi thời gian hết hạn';
$string['overduehandlingautoabandon'] = 'Cần nộp bài (submit) trước khi thời gian hết hạn nếu không sẽ không được chấp nhận';
$string['overduehandlinggraceperiod'] = 'Cho gia hạn thời gian nộp bài nhưng không được trả lời thêm câu hỏi.';
$string['quizclosed'] = 'Đề thi kết thúc lúc: {$a}';
$string['quiznavigation'] = 'Thanh điều khiển';
$string['returnattempt'] = 'Quay lại bài làm';
$string['stateinprogress'] = 'Đang tiến hành';
$string['status'] = 'Trạng thái';
$string['summaryofattempt'] = 'Tóm tắt bài làm';
$string['summaryofattempts'] = 'Tóm tắt các lần làm bài trước của bạn';
$string['timing'] = 'Thiết lập thời gian';
