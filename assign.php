<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://elearning.humg.edu.vn
 *
 * @package    mod
 * @subpackage assign
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addsubmission'] = 'Nộp bài';
$string['addsubmission_help'] = 'Bạn chưa nộp bài lần nào';
$string['allowsubmissionsfromdate'] = 'Ngày bắt đầu cho phép nộp bài';
$string['assign:addinstance'] = 'Thêm một bài tập mới';
$string['attemptreopenmethod'] = 'Cho phép làm lại';
$string['attemptreopenmethod_manual'] = 'Cho phép';
$string['attemptreopenmethod_none'] = 'Không cho phép';
$string['attemptreopenmethod_untilpass'] = 'Làm cho đến khi  vượt qua bài';
$string['availability'] = 'Thời gian nộp bài tập';
$string['calendardue'] = '{$a} quá hạn';
$string['changeuser'] = 'Đổi thành viên';
$string['cutoffdate'] = 'Hạn cuối cùng. (Hạn chót)';
$string['description'] = 'Mô tả';
$string['duedate'] = 'Hạn nộp bài';
$string['editsubmission_help'] = 'Bạn vẫn có thể thay đổi bài nộp';
$string['feedbacktypes'] = 'Các hình thức phản hồi';
$string['filesubmissions'] = 'Tệp tin nộp';
$string['filternotsubmitted'] = 'Không nộp';
$string['gradingduedate'] = 'Nhắc nhở giáo viên chấm điểm';
$string['gradingstatus'] = 'Trạng thái chấm điểm';
$string['gradingsummary'] = 'Tóm tắt chấm điểm';
$string['groupsubmissionsettings'] = 'Các thiết lập gửi bài theo nhóm';
$string['introattachments'] = 'Tập tin đính kèm';
$string['lastmodifiedgrade'] = 'Sửa lần cuối lúc (điểm)';
$string['lastmodifiedsubmission'] = 'Sửa lần cuối lúc (nộp bài)';
$string['maxattempts'] = 'Số lần làm lại tối đa';
$string['modulename'] = 'Bài tập';
$string['noattempt'] = 'Chưa nộp';
$string['notgraded'] = 'Chưa chấm điểm';
$string['notifications'] = 'Thiết lập thông báo';
$string['notsubmittedyet'] = 'Chưa nộp';
$string['numberofsubmissionsneedgrading'] = 'Cần chấm điểm';
$string['numberofsubmittedassignments'] = 'Đã nộp';
$string['removesubmission'] = 'Xóa bài nộp';
$string['removesubmissionconfirm'] = 'Bạn có chắc chắn muốn xóa bài nộp?';
$string['requiresubmissionstatement'] = 'Yên cầu sinh viên chấp nhận yêu cầu trước khi gửi bài';
$string['savechanges'] = 'Lưu';
$string['sendlatenotifications'] = 'Thông báo giáo viên về việc gửi bài muộn của sinh viên';
$string['sendnotifications'] = 'Thông báo giáo viên về việc gửi bài của sinh viên';
$string['sendstudentnotificationsdefault'] = 'Thiết lập mặc định "thông báo cho sinh viên"';
$string['submissiondrafts'] = 'Yêu cầu sinh viên ấn vào nút gửi bài (submit)';
$string['submissionempty'] = 'Không có gì được nộp';
$string['submissionsettings'] = 'Các thiết lập gửi bài';
$string['submissionstatus'] = 'Trạng thái nộp bài';
$string['submissionstatusheading'] = 'Trạng thái nộp bài';
$string['submissionstatus_submitted'] = 'Đã nộp để chấm điểm';
$string['submissiontypes'] = 'Các hình thức nộp bài';
$string['teamsubmission'] = 'Các sinh viên gửi bài theo nhóm';
$string['timemodified'] = 'Sửa lần cuối';
$string['timeremaining'] = 'Thời gian còn lại';
$string['timeremainingcolon'] = 'Thời gian còn lại: {$a}';
$string['unlimitedattemptsallowed'] = 'Số lần làm không giới hạn';
$string['viewgrading'] = 'Xem tất cả bài nộp';
$string['viewownsubmissionstatus'] = 'Xem thông tin nộp bài của bạn';
