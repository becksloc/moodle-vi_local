<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://elearning.humg.edu.vn
 *
 * @package    mod
 * @subpackage scorm
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['activation'] = 'Kích hoạt';
$string['activityoverview'] = 'Bạn có gói SCORM cần chú ý';
$string['activitypleasewait'] = 'Đang tải, vui lòng chờ';
$string['adminsettings'] = 'Thiết lập quản trị';
$string['advanced'] = 'Tham số';
$string['aicchacpkeepsessiondata'] = 'dữ liệu phiên AICC HACP';
$string['areacontent'] = 'Tệp nội dung';
$string['areapackage'] = 'Tệp đóng gói';
$string['attempt'] = 'Lần thử';
$string['attempt1'] = '1 lần thử';
$string['attempts'] = 'Lần thử';
$string['attemptsmanagement'] = 'Quản lý lần thử';
$string['attemptstatusall'] = 'Bảng điều khiển và trang nhập';
$string['attemptstatusentry'] = 'Chỉ trang chỉ mục';
$string['attemptstatusmy'] = 'Chỉ bảng điều khiển';
$string['attemptsx'] = '{$a} lần thử';
$string['autocontinue'] = 'Tự động tiếp tục';
$string['averageattempt'] = 'Lần thử trung bình';
$string['browse'] = 'Xem trước';
$string['browsed'] = 'Duyệt';
$string['browsemode'] = 'Chế độ xem trước';
$string['browserepository'] = 'Duyệt kho';
$string['calculatedweight'] = 'Trọng số tính';
$string['compatibilitysettings'] = 'Thiết lập các tùy chọn tương thích';
$string['completed'] = 'Hoàn thành';
$string['completionscorerequired'] = 'Yêu cầu điểm tối thiểu';
$string['completionscorerequireddesc'] = 'Điểm tối thiểu {$a} bắt buộc để hoàn thành';
$string['completionstatusrequired'] = 'Yêu cầu tình trạng';
$string['completionstatus_completed'] = 'Hoàn thành';
$string['completionstatus_passed'] = 'Đạt';
$string['contents'] = 'Nội dung';
$string['coursepacket'] = 'Đóng gói khóa học';
$string['coursestruct'] = 'Cấu trúc khóa học';
$string['currentwindow'] = 'Cửa sổ hiện tại';
$string['deleteallattempts'] = 'Xóa tất cả lần thử SCORM';
$string['deleteselected'] = 'Xóa lần thử được chọn';
$string['deleteuserattemptcheck'] = 'Bạn có chắc chắn muốn xóa tất cả các lần thử?';
$string['details'] = 'Chi tiết theo dõi';
$string['directories'] = 'Hiển thị liên kết thư mục';
$string['disabled'] = 'Vô hiệu hóa';
$string['display'] = 'Hiển thị gói';
$string['displayactivityname'] = 'Hiển thị tên hoạt động';
$string['displayattemptstatus'] = 'Hiển thị trạng thái thử';
$string['displaycoursestructure'] = 'Hiển thị cấu trúc khóa học trên trang chỉ mục';
$string['dnduploadscorm'] = 'Thêm một gói SCORM';
$string['duedate'] = 'Ngày hết hạn';
$string['enter'] = 'Tham gia';
$string['entercourse'] = 'Tham gia khóa học';
$string['eventattemptdeleted'] = 'Lần thử đã xóa';
$string['eventuserreportviewed'] = 'Báo cáo người dùng đã xem';
$string['everyday'] = 'Hằng ngày';
$string['everytime'] = 'Mỗi lần được sử dụng';
$string['exceededmaxattempts'] = 'Bạn đã thử đến giới hạn';
$string['firstattempt'] = 'Lần thử đầu tiên';
$string['floating'] = 'Nổi';
$string['forceattemptalways'] = 'luôn lôn';
$string['forceattemptoncomplete'] = 'Khi lần thử trước hoàn thành';
$string['forcecompleted'] = 'Cố gắng hoàn thành';
$string['forcenewattempts'] = 'Bắt buộc lần thử mới';
$string['gradeaverage'] = 'Điểm trung bình';
$string['gradeforattempt'] = 'Điểm cho lần thử';
$string['gradehighest'] = 'Điểm cao nhất';
$string['grademethod'] = 'Cách tính điểm';
$string['gradereported'] = 'Điểm tổng kết';
$string['gradesum'] = 'Điểm tổng';
$string['hidden'] = 'Ẩn';
$string['hidebrowse'] = 'Tắt chế độ xem trước';
$string['hidetoc'] = 'Hiển thị cấu trúc khóa học trong trình phát';
$string['highestattempt'] = 'Lần thử cao nhất';
$string['info'] = 'Thông tin';
$string['last'] = 'Truy cập cuối lúc';
$string['lastattempt'] = 'Lần thử hoàn thành cuối';
$string['lastattemptlock'] = 'Khóa sau lần thử cuối';
$string['location'] = 'Hiển thị thanh địa điểm';
$string['maximumattempts'] = 'Số lần thử';
$string['menubar'] = 'Hiển thị thanh thực đơn';
$string['myattempts'] = 'Lần thử của tôi';
$string['nav'] = 'Hiển thị thanh điều hướng';
$string['networkdropped'] = 'Kết nối Internet của bạn không đáng tin hoặc gián đoạn.<br /> Nếu bạn tiếp tục hoạt động SCORM này, tiến trình học của bạn có thể không được lưu lại. <br /> Bạn nên thoát khỏi hoạt động này ngay, và quay lại khi bạn có kết nối Internet phù hợp.';
$string['newattempt'] = 'Bắt đầu lần thử mới';
$string['noattemptsallowed'] = 'Số lần làm bài cho phép';
$string['noattemptsmade'] = 'Số lần bạn đã thử';
$string['nolimit'] = 'Không giới hạn lần thử';
$string['notattempted'] = 'Không thử';
$string['optallstudents'] = 'tất cả người dùng';
$string['optattemptsonly'] = 'chỉ người dùng đã thử';
$string['optnoattemptsonly'] = 'chỉ người dùng chưa thử';
$string['packagehdr'] = 'Gói bài giảng scorm';
$string['pagesize'] = 'Cỡ trang';
$string['popup'] = 'Cửa sổ mới';
$string['popupmenu'] = 'Trong menu thả xuống';
$string['preferencespage'] = 'Tùy chọn chỉ dành cho trang này';
$string['preferencesuser'] = 'Tùy chọn cho báo cáo này';
$string['privacy:metadata:attempt'] = 'Số lần thử';
$string['privacy:metadata:scorm_aicc_session'] = 'Thông tin phiên AICC HACP';
$string['report'] = 'Báo cáo';
$string['reportcountallattempts'] = '{$a->nbattempts} lần thử của {$a->nbusers} người dùng, trong số {$a->nbresults} kết quả';
$string['reports'] = 'Báo cáo';
$string['reviewmode'] = 'Chế độ xem lại';
$string['scorm:deleteownresponses'] = 'Xóa lần tự thử';
$string['scorm:deleteresponses'] = 'Xóa tất cả lần thử SCORM';
$string['scorm:viewreport'] = 'Xem báo cáo';
$string['scormclose'] = 'Thời gian kết thúc';
$string['scormopen'] = 'Thời gian bắt đầu';
$string['scormresponsedeleted'] = 'Đã xóa lần thử người dùng';
$string['search:activity'] = 'Thông tin hoạt động gói SCORM';
$string['show'] = 'Hiển thị';
$string['sided'] = 'Sang một bên';
$string['started'] = 'Bắt đầu lúc';
$string['statusbar'] = 'Hiển thị thanh trạng thái';
$string['subplugintype_scormreport'] = 'Báo cáo';
$string['subplugintype_scormreport_plural'] = 'Báo cáo';
$string['toolbar'] = 'Hiển thị thanh công cụ';
$string['undercontent'] = 'Phía dưới nội dung';
$string['updatefreq'] = 'Tần suất cập nhật tự động gói bài giảng SCORM';
$string['viewallreports'] = 'Xem báo cáo lần thử {$a}';
$string['viewalluserreports'] = 'Xem báo cáo người dùng {$a}';
$string['whatgrade'] = 'Chấm điểm lần thử';
