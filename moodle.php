<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://elearning.humg.edu.vn
 *
 * @package    core
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['hidefromstudents'] = 'Ẩn';
$string['lastcourseaccess'] = 'Lần truy cập cuối';
$string['lastsiteaccess'] = 'Lần truy cập cuối';
$string['messageprovider:gradenotifications'] = 'Các thông báo điểm';
$string['myhome'] = 'Bảng điều khiển';
$string['never'] = 'Không bao giờ';
$string['nofiltersapplied'] = 'Chưa lọc';
$string['participantscount'] = 'Số thành viên: {$a}';
$string['privatefiles'] = 'Tệp tin cá nhân';
$string['privatefilesmanage'] = 'Quản lý Tệp tin cá nhân';
$string['resettable'] = 'Đặt lại tùy chọn';
$string['savechanges'] = 'Lưu';
$string['showchartdata'] = 'Hiển thị dữ liệu biểu đồ';
$string['showoncoursepage'] = 'Hiển thị trên trang khóa học';
$string['sitehome'] = 'Trang chủ';
$string['userfilterplaceholder'] = 'Gõ từ khóa hoặc nhấn chọn';
$string['youhaveupcomingactivitiesdue'] = 'Bạn có các hoạt động sắp quá hạn';
$string['youhaveupcomingactivitiesdueinfo'] = 'Chào {$a}, <br/><br/>Bạn có các hoạt động sắp quá hạn:';
