<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local language pack from http://elearning.humg.edu.vn
 *
 * @package    tool
 * @subpackage lp
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['competencyframeworks'] = 'Khung năng lực';
$string['competencyframeworksrepository'] = 'Kho các Khung năng lực';
$string['configurecoursecompetencysettings'] = 'Cấu hình khung năng lực';
$string['coursecompetencies'] = 'Khung năng lực';
$string['filterbyactivity'] = 'Lọc theo tài nguyên hoặc hoạt động';
$string['listcompetencyframeworkscaption'] = 'Danh sách Khung năng lực';
$string['modcompetencies'] = 'Khung năng lực';
$string['modcompetencies_help'] = 'Khung năng lực liên kết tới hoạt động này';
$string['nocompetenciesincourse'] = 'Không có khung năng lực nào được liên kết tới khóa học này';
$string['nocompetencyframeworks'] = 'Không có Khung năng lực nào được tạo';
